import Vue from 'vue'
import ToolBar from './ToolBar.vue'

export default {
  install (Vue, options) {
    const axios = options
    let loaderInstance = new Vue({
      data () {
        return {
          request: '',
          response: ''
        }
      }
    })
    
    
    Vue.component('toolbar', ToolBar)
    Vue.prototype.$loader = loaderInstance
    Vue.prototype.$axios = axios
  }
}