# Looker utils

Setup the project using this plugins:

In src/api/utils.js:
 - Change "let instance = axios.create({" to "export let instance = axios.create({"

In main.js: 
 - Add "import { instance } from '@/api/utils'"
 - Add "import LookerUtils from '../node_modules/looker-utils'"
 - Add "Vue.use(LookerUtils, instance)"

In App.vue:
 - Add "<toolbar />" below the main div